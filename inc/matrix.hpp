#pragma once
#include <iostream>

namespace mt::math
{
	template<typename T, int N, int M>
	struct MasWrapper
	{
		T mas[N][M];
	};

	template<typename T, int P, int Q>
	struct MasWrapper2
	{
		T mas[P][Q];
	};

	template<typename T, int N, int M>
	class Matrix
	{
	public:
		Matrix()
		{
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = 0;
		}

		Matrix(const T mas[N][M])
		{
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas[i][j];
		}

		Matrix(const MasWrapper<T, N, M>& mas)
		{
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas.mas[i][j];
		}

		Matrix(const Matrix<T, N, M>& mat)
		{
			m_n = mat.m_n;
			m_m = mat.m_m;

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.m_mat[i][j];
		}

		int getN() const { return m_n; }
		int getM() const { return m_m; }
		T get(int i, int j) const { return m_mat[i][j]; }
		void set(int i, int j, T data) { m_mat[i][j] = data; }

		template<typename T, int N, int M>
		Matrix<T, N, M>& operator=(const Matrix<T, N, M>& mat)
		{
			m_n = mat.getN();
			m_m = mat.getM();

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.get(i, j);

			return *this;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator+(const Matrix<T, N, M>& mat)
		{
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
			return tmp;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator-(const Matrix<T, N, M>& mat)
		{
			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
			return tmp;
		}

		template<typename T, int P, int Q>
		Matrix<T, N, Q> operator*(const Matrix<T, P, Q>& mat)
		{
			Matrix<T, N, Q> tmp;
			if (M == P)
			{
				for (int i = 0; i < m_n; i++)
					for (int j = 0; j < mat.getM(); j++)
					{
						T sum = 0;
						for (int k = 0; k < m_m; k++)
							sum += m_mat[i][k] * mat.get(k, j);
						tmp.set(i, j, sum);
					}
			}
			else {
				throw std::exception("Wrong matrix size!");
			}
			return tmp;
		}

		T det()
		{
			T result = 0;

			if (m_n == 2 && m_m == 2)
			{
				result = m_mat[0][0] * m_mat[1][1] -
					m_mat[0][1] * m_mat[1][0];
			}
			else if (m_n == 3 && m_m == 3)
			{
				result = m_mat[0][0] * m_mat[1][1] * m_mat[2][2] - m_mat[0][0] * m_mat[1][2] * m_mat[2][1]
					- m_mat[0][1] * m_mat[1][0] * m_mat[2][2] + m_mat[0][1] * m_mat[1][2] * m_mat[2][0]
					+ m_mat[0][2] * m_mat[1][0] * m_mat[2][1] - m_mat[0][2] * m_mat[1][1] * m_mat[2][0];
			}
			else
			{
				throw std::exception("Wrong matrix!");
			}

			return result;
		}

		Matrix<T, N, M> inv()
		{
			Matrix<T, N, M> tmp;

			int d = det();
			if (d == 0)
				throw std::exception("Zero determinant!");

			if (m_n == 2 && m_m == 2)
			{
				tmp.set(0, 0, m_mat[1][1] / d);
				tmp.set(1, 0, m_mat[0][1] / d * -1);
				tmp.set(0, 1, m_mat[1][0] / d * -1);
				tmp.set(1, 1, m_mat[0][0] / d);
			}
			else if (m_n == 3 && m_m == 3)
			{
				Matrix<double, 2, 2> tz;
				int c = 0;
				for (int i = 0; i < 3; i++)
					for (int j = 0; j < 3; j++)
					{
						for (int k = 0; k < 3; k++)
						{
							for (int l = 0; l < 3; l++)
							{
								if (i != k && j != l) {
									switch (c) {
									case 0:
										tz.set(0, 0, m_mat[k][l]);
										c++;
										break;
									case 1:
										tz.set(0, 1, m_mat[k][l]);
										c++;
										break;
									case 2:
										tz.set(1, 0, m_mat[k][l]);
										c++;
										break;
									case 3:
										tz.set(1, 1, m_mat[k][l]);
										c = 0;
										break;
									}
								}
							}
						}

						double dett = tz.det();
						if ((i + 1 + j + 1) % 2 != 0)
							dett *= -1;

						tmp.set(i, j, dett / d);
					}


			}
			else
				throw std::exception("Wrong matrix!");


			return tmp;
		}
		Matrix<T, N, M> transpose()
		{
			Matrix<T, N, M> tmp;

			for (int i = 0; i < m_n; i++)
			{
				for (int j = 0; j < m_m; j++)
				{
					tmp.set(i, j, m_mat[j][i]);
				}
			}

			return tmp;
		}

		~Matrix()
		{
#ifdef MY_DEBUG
			std::cout << "Destructor" << std::endl;
#endif
		}

		template<typename T, int N, int M>
		friend std::istream& operator>>(std::istream& os, Matrix<T, N, M>& mat);

		template<typename T, int N, int M>
		friend std::ostream& operator<<(std::ostream& os, const Matrix<T, N, M>& mat);

	private:
		int m_n, m_m;
		T m_mat[N][M];
	};

	template<typename T, int N, int M>
	std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat)
	{
		for (int i = 0; i < mat.m_n; i++)
			for (int j = 0; j < mat.m_m; j++)
				in >> mat.m_mat[i][j];
		return in;
	}

	template<typename T, int N, int M>
	std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat)
	{
		out << "Matrix " << mat.m_n << "x" << mat.m_m << std::endl;
		for (int i = 0; i < mat.m_n; i++) {
			for (int j = 0; j < mat.m_m; j++)
				out << mat.m_mat[i][j] << " ";
			out << std::endl;
		}
		return out;
	}

	using Vec2i = Matrix<int, 2, 1>;
	using Vec2d = Matrix<double, 2, 1>;
	using Vec3d = Matrix<double, 3, 1>;

	using Mat22i = Matrix<int, 2, 2>;
	using Mat22d = Matrix<double, 2, 2>;
	using Mat33d = Matrix<double, 3, 3>;
}

