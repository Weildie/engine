#pragma once
#include <iostream>
#include <matrix.hpp>

namespace mt
{
	using math::Vec2i;
	using math::Vec3d;
	using math::Mat33d;

	struct Point
	{
		double x, y, z;
	};

	struct Angles 
	{
		double roll, pitch, yaw;
	};

	struct Pixel
	{
		uint8_t r, g, b, a;
	};

	struct Intrinsic
	{
		double fu, fv;
		double du, dv;
	};

	class Camera
	{
	private:
		int m_width;
		int m_height;
		Pixel* m_picture;
		Point* m_originalCords;
		Intrinsic m_intrinsic;

		Point m_position;
		Angles m_angles;

		Pixel getColor(int i, int j);
		void average(int& r, int& g, int& b, int i, int j, int& cnt);
	public:
		Camera(int width, int height, Intrinsic intrinsic, Point position, Angles angles);
		~Camera();
		Pixel* picture();
		void fill(Pixel pixel);
		void clear();
		int getPointsQuantity();
		void projectPoint(Point p, Pixel c);
		void interpolate();
		void setPos(Point p);
		Point& getPos();
		bool isNone(int i, int j);
		Point getXYZ(int x, int y);
		Vec2i getXY(Point p);

		void dX(double d);
		void dZ(double d);
		void dRoll(double droll);
		void dPitch(double dpitch);
	};
}