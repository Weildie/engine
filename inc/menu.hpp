#pragma once 

namespace mt
{
	class Menu {
	private:
		std::unique_ptr<sf::RectangleShape> m_shape;
		std::unique_ptr<sf::Font> m_font;
		std::unique_ptr<sf::Text> m_fpsCounter;
		std::array<std::unique_ptr<sf::Text>, 3> m_textCords;
		std::unique_ptr<sf::Text> m_pointsCounter;
		std::unique_ptr<sf::Text> m_squareSelected;
	public:
		void setMenuSection(std::unique_ptr<sf::Text>& section, int i);
		Menu();
		sf::RectangleShape& get();
		std::vector<sf::Text> getMenuSection();
		void update(int fps, Point camera_cords, int points, double square);
	};

}