#pragma once
#include <SFML/Graphics.hpp>
#include <camera.hpp>
#include <array>

//
// I write Line class here because it works close with class Marker and does the same things
//

namespace mt
{
	class Square
	{
	private:
		std::shared_ptr<sf::ConvexShape> m_convex;
		std::vector<Point> m_points;
		double m_square;
	public:
		Square(std::vector<Point> init_vec);
		void update(Point& new_point);
		sf::ConvexShape& getConvex();
		double getTriangleSquare(std::array<Point, 3> points);
		double getSquare();
	};

	class Line
	{
	private:
		std::array<std::unique_ptr<sf::Vertex>, 2> m_line;
		std::unique_ptr<sf::Text> m_text;
		std::unique_ptr<sf::Font> m_font;
		Point m_point1;
		Point m_point2;
		double m_length;
	public:
		Line(Point& m1, Point& m2);
		std::array<sf::Vertex, 2>& getLine();
		std::array<Point, 2> getPointsXYZ();
		sf::Text& getText();
	};

	class Marker
	{
	private:
		std::unique_ptr<sf::RectangleShape> m_shape;
		std::unique_ptr<sf::Text> m_text;
		std::unique_ptr<sf::Font> m_font;
		Point m_coordinates;
		std::weak_ptr<Marker> m_parent;
	public:
		Marker(Point& p);
		~Marker();
		sf::RectangleShape& getRect();
		sf::Text& getText();
		Point getXYZ();
		friend bool link(std::shared_ptr<Marker>& target1, std::shared_ptr<Marker>& target2);
		std::shared_ptr<Marker> Marker::getParent();
	};
}