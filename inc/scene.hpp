#pragma once
#include <marker.hpp>
#include <menu.hpp>

namespace mt
{
	class Scene
	{
	private:
		std::unique_ptr<Camera> m_camera;
		int m_width;
		int m_height;
		std::unique_ptr<sf::RenderWindow> m_window;
		std::unique_ptr<sf::Texture> m_texture;
		std::unique_ptr<sf::Sprite> m_sprite;
		std::vector<Point> m_points;
		std::vector<Pixel> m_colors;
		std::unique_ptr<Menu> m_menu;
		std::vector<std::shared_ptr<Marker>> m_markers;
		std::vector<std::shared_ptr<Line>> m_lines;
		std::unique_ptr<Square> m_square;
		Point findPointInRad(int x, int y);
		int m_size = 0;
	public:
		Scene(int width, int height);
		~Scene();
		void initWithFile(std::string destination);
		void initEllipse();
		void projectMarker(int i);
		void projectLineFromMarker(int i);
		void lifeCycle();
	};
}