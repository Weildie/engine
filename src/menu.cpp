#include <camera.hpp>
#include <iostream>
#include <array>
#include <SFML/Graphics.hpp>
#include <menu.hpp>

namespace mt 
{
	void Menu::setMenuSection(std::unique_ptr<sf::Text>& section, int i)
	{
		section = std::make_unique<sf::Text>();
		section->setFont(*m_font);
		section->setCharacterSize(24);
		section->setString(std::to_string(0));
		section->setPosition(20, 15 + i * 30);
	}

	Menu::Menu()
	{
		m_shape = std::make_unique<sf::RectangleShape>(sf::Vector2f(250, 195));
		m_shape->setFillColor(sf::Color(0, 3, 181, 100));
		m_shape->setPosition(10, 10);
		m_shape->setOutlineThickness(2);
		m_shape->setOutlineColor(sf::Color(0, 3, 181, 200));

		m_font = std::make_unique<sf::Font>();
		if (!m_font->loadFromFile("../assets/consolas.ttf"))
			std::cout << "Font loading error!" << std::endl;

		setMenuSection(m_fpsCounter, 0);
		setMenuSection(m_pointsCounter, 1);
		setMenuSection(m_textCords[0], 2);
		setMenuSection(m_textCords[1], 3);
		setMenuSection(m_textCords[2], 4);
		setMenuSection(m_squareSelected, 5);
	}

	sf::RectangleShape& Menu::get()
	{
		return *m_shape;
	}

	std::vector<sf::Text> Menu::getMenuSection()
	{
		std::vector<sf::Text> tmp = { *m_fpsCounter, *m_pointsCounter, *m_textCords[0], *m_textCords[1], *m_textCords[2], *m_squareSelected };
		return tmp;
	}

	void Menu::update(int fps, Point camera_cords, int points, double square)
	{
		m_fpsCounter->setString("FPS: " + std::to_string(fps));
		m_textCords[0]->setString("X: " + std::to_string(camera_cords.x));
		m_textCords[1]->setString("Y: " + std::to_string(camera_cords.y));
		m_textCords[2]->setString("Z: " + std::to_string(camera_cords.z));
		m_pointsCounter->setString("POS: " + std::to_string(points));
		m_squareSelected->setString("Square: " + std::to_string(square));
	}
}