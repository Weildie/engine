#pragma once
#include <scene.hpp>
#include <fstream>
#include <cassert>

mt::Scene::Scene(int width, int height)
{
	m_width = width;
	m_height = height;
	m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(m_width, m_height), "Engine");
	m_texture = std::make_unique<sf::Texture>();
	m_texture->create(m_width, m_height);
	m_sprite = std::make_unique<sf::Sprite>(*m_texture);

	m_menu = std::make_unique<Menu>();

	Intrinsic intrinsic({960, 540, 960, 640});
	Point position = { 0.0, 0.0, 0.0 };
	Angles angles = { 0.0, 0.0, 0.0 }; // 1.8 for file
	m_camera = std::make_unique<Camera>(m_width, m_height, intrinsic, position, angles);
}

mt::Scene::~Scene() 
{
	//
};

void mt::Scene::initWithFile(std::string destination)
{
	// 467361.850, -50.520, 6063517.300 for input file
	m_camera->setPos({ 467361.850, -50.520, 6063517.300 });

	std::ifstream in(destination);
	double x, y, z;
	int r, g, b;
	m_size = 0;
	while (!in.eof())
	{
		in >> x >> y >> z >> r >> g >> b;
		m_points.push_back({ x, y, z });
		m_colors.push_back({ static_cast<uint8_t>(r), static_cast<uint8_t>(g), static_cast<uint8_t>(b), static_cast<uint8_t>(255) });
		m_size++;
	}
	in.close();
}

void mt::Scene::initEllipse()
{
	double r = 1;
	for (double fi = 0; fi < 6.28; fi += 0.01)
		for (double teta = 0; teta < 1.57; teta += 0.01)
		{
			m_points.push_back({
				(r* sin(teta)* cos(fi)),
				(r* sin(teta)* sin(fi) + 5),
				(r* cos(teta))
			});
			m_size++;
		}
}

mt::Point mt::Scene::findPointInRad(int x, int y) 
{
	for (int i = y - 4; i < y + 4; i++)
		for (int j = x - 4; j < x + 4; j++)
			if (!m_camera->isNone(j, i))
				return m_camera->getXYZ(j, i);

	// TODO exceptions in case of click on isNone()
	assert("No points in radius!");
}

void mt::Scene::projectMarker(int i)
{
	Vec2i pos = m_camera->getXY(m_markers[i]->getXYZ());
	int u = pos.get(0, 0);
	int v = pos.get(1, 0);

	m_markers[i]->getRect().setPosition(u, v);
	m_markers[i]->getText().setPosition(u + 5, v - 5);
	m_window->draw(m_markers[i]->getRect());
	m_window->draw(m_markers[i]->getText());
}

void mt::Scene::projectLineFromMarker(int i)
{
	if (i < m_markers.size() - 1)
	{
		auto tmp = m_lines[i]->getLine();
		sf::Vertex line[2] = { tmp[0], tmp[1] };

		auto p1_2d = m_camera->getXY(m_lines[i]->getPointsXYZ()[0]);
		auto p2_2d = m_camera->getXY(m_lines[i]->getPointsXYZ()[1]);

		line[0].position = sf::Vector2f(p1_2d.get(0, 0), p1_2d.get(1, 0));
		line[1].position = sf::Vector2f(p2_2d.get(0, 0), p2_2d.get(1, 0));

		auto text = m_lines[i]->getText();
		text.setPosition((p1_2d.get(0, 0) + p2_2d.get(0, 0)) / 2, (p1_2d.get(1, 0) + p2_2d.get(1, 0)) / 2);

		m_window->draw(line, 2, sf::Lines);
		m_window->draw(text);
	}
}

void mt::Scene::lifeCycle()
{
	clock_t current_ticks, delta_ticks;
	clock_t fps = 0;

	while (m_window->isOpen())
	{
		current_ticks = clock();

		sf::Event event;
		while (m_window->pollEvent(event))
			if (event.type == sf::Event::Closed)
				m_window->close();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			m_camera->dZ(0.1);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			m_camera->dZ(-0.1);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			m_camera->dX(-0.1);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			m_camera->dX(0.1);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			m_camera->dPitch(-0.02);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			m_camera->dPitch(0.02);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			m_camera->dRoll(-0.02);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			m_camera->dRoll(0.02);

		// for diffrent inits
		if (m_colors.size() != 0)
		{
			for (int i = 0; i < m_size; i++)
				m_camera->projectPoint(m_points[i], m_colors[i]);
		}
		else
		{
			for (int i = 0; i < m_size; i++)
				m_camera->projectPoint(m_points[i], {255, 255, 255, 255});
		}
		
		// m_camera->interpolate(); // (just for show BUGS)
		// update main matrix
		m_texture->update((uint8_t*)m_camera->picture(), 1920, 1080, 0, 0);

		double tmp_square = m_square != NULL ? m_square->getSquare() : 0;
		m_menu->update(fps, m_camera->getPos(), m_camera->getPointsQuantity(), tmp_square);

		// event for markers
		if (event.type == sf::Event::MouseButtonPressed)
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				int X = event.mouseButton.x;
				int Y = event.mouseButton.y;

				auto tmp = std::make_shared<Marker>(findPointInRad(X, Y));

				if (m_markers.size() > 0)
				{
					link(tmp, m_markers[m_markers.size() - 1]);
					m_lines.push_back(std::make_shared<Line>(tmp->getXYZ(), tmp->getParent()->getXYZ()));
				}

				m_markers.push_back(tmp);

				// square
				if (m_markers.size() < 2)
				{
					m_square = NULL;
				}
				else if (m_markers.size() > 2 && m_square == NULL)
				{
					std::vector<Point> tmp_points;

					for (int i = 0; i < m_markers.size(); i++)
						tmp_points.push_back(m_markers[i]->getXYZ());

					m_square = std::make_unique<Square>(tmp_points);
				}
				else if (m_markers.size() > 2 && m_square != NULL)
				{
					m_square->update(m_markers[m_markers.size()-1]->getXYZ());
				}

			}
			if (event.mouseButton.button == sf::Mouse::Right)
			{
				if (m_markers.size() > 0)
				{
					m_markers.pop_back();

					if (m_lines.size() > 0)
						m_lines.pop_back();

					// square deleteOne()
				}
			}

		m_camera->clear();
		m_window->clear();

		m_window->draw(*m_sprite);

		// 3D -> 2D -> draw (markers, lines)
		if (m_markers.size() > 0)
			for (int i = 0; i < m_markers.size(); i++)
			{
				projectMarker(i);
				projectLineFromMarker(i);
			}

		// square
		if (m_markers.size() > 2 && m_square != NULL)
		{
			m_square->getConvex().setPointCount(m_markers.size());
			for (int i = 0; i < m_markers.size(); i++)
			{
				Vec2i pos = m_camera->getXY(m_markers[i]->getXYZ());
				m_square->getConvex().setPoint(i, sf::Vector2f(static_cast<float>(pos.get(0, 0)), static_cast<float>(pos.get(1, 0))));
			}
			m_window->draw(m_square->getConvex());
		}

		// menu
		m_window->draw(m_menu->get());
		auto sections = m_menu->getMenuSection();
		for (int i = 0; i < sections.size(); i++)
			m_window->draw(sections[i]);

		m_window->display();

		delta_ticks = clock() - current_ticks;
		if (delta_ticks > 0)
			fps = CLOCKS_PER_SEC / delta_ticks;
	}
}