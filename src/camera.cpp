#include <camera.hpp>

namespace mt 
{
	Camera::Camera(int width, int height, Intrinsic intrinsic, Point position, Angles angles)
	{
		m_width = width;
		m_height = height;
		m_intrinsic = intrinsic;
		m_picture = new Pixel[m_width * m_height];
		m_originalCords = new Point[m_width * m_height];
		m_position = position;
		m_angles = angles;
		clear();
	}

	Camera::~Camera()
	{
		delete[] m_picture;
		delete[] m_originalCords;
	}

	Pixel* Camera::picture() { return m_picture; }

	void Camera::fill(Pixel pixel)
	{
		for (int i = 0; i < m_height; i++)
			for (int j = 0; j < m_width; j++)
				m_picture[i * m_width + j] = pixel;
	}

	void Camera::clear()
	{
		fill({ 0, 0, 0, 255 });
		delete[] m_originalCords;
		m_originalCords = new Point[m_width * m_height];
	}

	int Camera::getPointsQuantity()
	{
		int c = 0;

		for (int i = 1; i < m_height - 1; i++)
			for (int j = 1; j < m_width - 1; j++)
				if (!isNone(j, i))
					c++;

		return c;
	}

	Vec2i mt::Camera::getXY(mt::Point p)
	{
		// rotate cords system for 90deg
		double X = p.x;
		double Y = -p.z;
		double Z = p.y;

		double a = m_angles.roll;
		double b = m_angles.pitch;
		double g = m_angles.yaw;

		double cosa = cos(a);
		double cosb = cos(b);
		double sina = sin(a);
		double sinb = sin(b);

		// to rotate camera cords system to O
		// ROTATE (OPTIMIZED)
		Mat33d R({ {
			{cosb, 0, -sinb},
			{-sina * sinb, cosa, -sina * cosb},
			{cosa * sinb, sina, cosa * cosb}
		} });

		// elements to camera cords system
		Vec3d P({ {
			{X - m_position.x},
			{Y - m_position.y},
			{Z - m_position.z}
		} });

		Vec3d P_res;

		P_res = R * P;

		double x = P_res.get(0, 0);
		double y = P_res.get(1, 0);
		double z = P_res.get(2, 0);

		// before the camera
		// TODO works wierd
		if (z <= 0)
		{
			Vec2i err({ {
				{-1},
				{-1}
			} });
			return err;
		}

		// (3D->2D)
		double u = x / z;
		double v = y / z;

		// (3D->2D) stage 2
		u = u * m_intrinsic.fu + m_intrinsic.du;
		v = v * m_intrinsic.fv + m_intrinsic.dv;

		Vec2i tmp({ { 
			{static_cast<int>(u)}, 
			{static_cast<int>(v)}
		} });

		return tmp;
	}


	void mt::Camera::projectPoint(mt::Point p, mt::Pixel c)
	{
		Vec2i tmp = getXY(p);

		int u = tmp.get(0, 0);
		int v = tmp.get(1, 0);

		if (u >= 0 && u < m_width && v >= 0 && v < m_height)
		{
			m_picture[(int)v * m_width + (int)u] = c;
			m_originalCords[(int)v * m_width + (int)u] = p;
		}
	}

	void Camera::setPos(Point p)
	{
		m_position = p;
	}

	Point& Camera::getPos() {
		return m_position;
	}

	Point Camera::getXYZ(int x, int y)
	{
		return m_originalCords[y * m_width + x];
	}

	bool Camera::isNone(int i, int j)
	{
		return m_picture[j * m_width + i].r == 0 && m_picture[j * m_width + i].g == 0 && m_picture[j * m_width + i].b == 0;
	}

	Pixel Camera::getColor(int i, int j)
	{
		return m_picture[j * m_width + i];
	}

	void Camera::average(int& r, int& g, int& b, int i, int j, int& cnt)
	{
		if (!isNone(i, j))
		{
			r += getColor(i, j).r;
			g += getColor(i, j).g;
			b += getColor(i, j).b;
			cnt++;
		}
	}

	void Camera::interpolate()
	{
		for (int i = 1; i < m_width - 1; i++)
			for (int j = 1; j < m_height - 1; j++)
				if (isNone(i, j))
				{
					int r = 0, g = 0, b = 0, cnt = 0;

					average(r, g, b, i, j-1, cnt);
					average(r, g, b, i, j + 1, cnt);

					average(r, g, b, i + 1, j - 1, cnt);
					average(r, g, b, i + 1, j, cnt);
					average(r, g, b, i + 1, j + 1, cnt);
					average(r, g, b, i + 1, j + 1, cnt);

					average(r, g, b, i-1, j, cnt);
					average(r, g, b, i-1, j+1, cnt);
					average(r, g, b, i - 1, j - 1, cnt);

					if (cnt != 0)
					{
						r /= cnt;
						g /= cnt;
						b /= cnt;

						m_picture[j * m_width + i].r = r;
						m_picture[j * m_width + i].g = g;
						m_picture[j * m_width + i].b = b;
						m_picture[j * m_width + i].a = 255;
					}
				}
	}

	void Camera::dX(double d)
	{
		m_position.x += d * cos(-m_angles.pitch);
		m_position.y += 0;
		m_position.z += d * sin(-m_angles.pitch);
	}

	void Camera::dZ(double d)
	{
		m_position.x += d * sin(m_angles.pitch);
		m_position.y += d * sin(m_angles.roll);
		m_position.z += d * cos(m_angles.pitch);
	}

	void Camera::dRoll(double droll)
	{
		m_angles.roll += droll;
	}

	void Camera::dPitch(double dpitch)
	{
		m_angles.pitch += dpitch;
	}


}
