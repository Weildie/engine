#include <marker.hpp>

namespace mt 
{
	Square::Square(std::vector<Point> init_vec)
	{
		for (int i = 0; i < init_vec.size(); i++)
			m_points.push_back(init_vec[i]);

		m_convex = std::make_shared<sf::ConvexShape>();
		m_convex->setFillColor(sf::Color(255, 0, 255, 100));

		std::array<Point, 3> tmp = { init_vec[0], init_vec[1], init_vec[2] };
		m_square = getTriangleSquare(tmp);
	}

	void Square::update(Point& new_point)
	{
		m_points.push_back(new_point);

		std::array<Point, 3> tmp = { m_points[0], m_points[m_points.size() - 2], m_points[m_points.size() - 1] };
		m_square += getTriangleSquare(tmp);
	}

	sf::ConvexShape& Square::getConvex()
	{
		return *m_convex;
	}

	double Square::getTriangleSquare(std::array<Point, 3> points)
	{
		// Heron's method
		double l1 = sqrt(pow((points[1].x - points[0].x), 2) + pow((points[1].y - points[0].y), 2) + pow((points[1].z - points[0].z), 2));
		double l2 = sqrt(pow((points[2].x - points[1].x), 2) + pow((points[2].y - points[1].y), 2) + pow((points[2].z - points[1].z), 2));
		double l3 = sqrt(pow((points[0].x - points[2].x), 2) + pow((points[0].y - points[2].y), 2) + pow((points[0].z - points[2].z), 2));
		double p_half = (l1 + l2 + l3) / 2;
		return sqrt(p_half * (p_half - l1) * (p_half - l2) * (p_half - l3));
	}

	double Square::getSquare()
	{
		return m_square;
	}


	// ^
	// | Square end

	Line::Line(Point& m1, Point& m2)
	{
		m_point1 = m1;
		m_point2 = m2;

		m_font = std::make_unique<sf::Font>();
		if (!m_font->loadFromFile("../assets/consolas.ttf"))
			std::cout << "Font loading error!" << std::endl;

		m_text = std::make_unique<sf::Text>();
		m_text->setFont(*m_font);
		m_text->setCharacterSize(24);
		m_text->setFillColor(sf::Color::White);
		m_length = sqrt(pow((m_point2.x - m_point1.x), 2) + pow((m_point2.y - m_point1.y), 2) + pow((m_point2.z - m_point1.z), 2));
		m_text->setString(std::to_string(m_length));

		m_line[0] = std::make_unique<sf::Vertex>();
		m_line[1] = std::make_unique<sf::Vertex>();
		m_line[0]->color = sf::Color::Magenta;
		m_line[1]->color = sf::Color::Magenta;

	}

	std::array<sf::Vertex, 2>& Line::getLine()
	{
		std::array<sf::Vertex, 2> tmp;
		tmp[0] = *m_line[0];
		tmp[1] = *m_line[1];
		return tmp;
	}

	std::array<Point, 2> Line::getPointsXYZ()
	{
		std::array<Point, 2> tmp = { m_point1, m_point2 };
		return tmp;
	}

	sf::Text& Line::getText()
	{
		return *m_text;
	}

	// ^
	// | Line end

	Marker::Marker(Point& p)
	{
		m_shape = std::make_unique<sf::RectangleShape>(sf::Vector2f(12, 12));
		m_shape->setFillColor(sf::Color::Magenta);
		m_shape->setOrigin(6, 6);

		m_font = std::make_unique<sf::Font>();
		if (!m_font->loadFromFile("../assets/consolas.ttf"))
			std::cout << "Font loading error!" << std::endl;

		m_text = std::make_unique<sf::Text>();
		m_text->setFont(*m_font);
		m_text->setCharacterSize(24);
		m_text->setFillColor(sf::Color::Magenta);

		std::string text = std::to_string(p.x) + " " + std::to_string(p.y) + " " + std::to_string(p.z);
		m_text->setString(text);

		m_coordinates = p;
	}

	Marker::~Marker()
	{
		// all markers deleted
		// std::cout << "Marker deleted" << std::endl;
	}

	sf::RectangleShape& Marker::getRect()
	{
		return *m_shape;
	}

	sf::Text& Marker::getText()
	{
		return *m_text;
	}

	Point Marker::getXYZ()
	{
		return m_coordinates;
	}

	std::shared_ptr<Marker> Marker::getParent()
	{
		return m_parent.lock();
	}

	bool mt::link(std::shared_ptr<Marker>& child, std::shared_ptr<Marker>& parent)
	{
		if (!child || !parent)
			return false;

		child->m_parent = parent;
	}
}