#include <iostream>
#include <scene.hpp>

int main() 
{
	mt::Scene scene(1920, 1080);
	scene.initWithFile("../points.txt");
	scene.lifeCycle();
	return 0;
}